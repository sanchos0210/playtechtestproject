import exeptions.JsonParsingException;
import io.restassured.response.Response;
import models.Employee;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.DataGenerator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ApiTests extends BaseTest {

    private EmployeeApiManager apiManager = new EmployeeApiManager();


    @Test
    public void employeeCreationTest() throws IOException, JsonParsingException {
        //employee creation
        List<Employee> employeeToCreate = new ArrayList();
        employeeToCreate.add(context.getFirst().setName(context.getFirst().getName() + DataGenerator.makeRandomName()));  //name of employee must be unique
        employeeToCreate.add(context.getSecond().setName(context.getSecond().getName() + DataGenerator.makeRandomName()));
        employeeToCreate.add(context.getThird().setName(context.getThird().getName() + DataGenerator.makeRandomName()));

        List<Employee> createdEmployee = apiManager.createEmployees(employeeToCreate);

        //verifying of created employee
        List<String> createdEmployeesId = new ArrayList();
        for(Employee e: createdEmployee) {
            createdEmployeesId.add(e.getId());
        }
        List<Employee> receivedEmployeesById = apiManager.getEmployees(createdEmployeesId);

        //employeeToCreate haven`t id because it got id after creation
        employeeToCreate.get(0).setId(receivedEmployeesById.get(0).getId());
        employeeToCreate.get(1).setId(receivedEmployeesById.get(1).getId());
        employeeToCreate.get(2).setId(receivedEmployeesById.get(2).getId());

        Assert.assertEquals(receivedEmployeesById, employeeToCreate);

        //saving created employee
        new Serializer<Employee>().writeToFile(new File("src/test/resources/created_employee.json"), receivedEmployeesById);
    }

    @Test
    public void changingRomanDataTest() throws IOException, JsonParsingException {
        //creation of new employee
        Employee employeeRoman = apiManager.createEmployee(context.getSecond().setName(context.getSecond().getName() + DataGenerator.makeRandomName()));
        //changing data for employee
        employeeRoman.setName("Not Roman" + DataGenerator.makeRandomName());
        employeeRoman.setAge("13");
        employeeRoman.setEmployeeSalary("0");

        //updated employee data
        Employee updatedEmployeeRoman = apiManager.updateEmployeeData(employeeRoman);

        //verifying of changed data
        Assert.assertEquals(employeeRoman, updatedEmployeeRoman);
    }

    @Test
    public void deleteEmployeeTest() throws IOException, JsonParsingException {
        //creation of new employee
        Employee employeeSlava = apiManager.createEmployee(context.getSecond().setName(context.getThird().getName() + DataGenerator.makeRandomName()));

        //delete employee
        Response response = apiManager.deleteEmployee(employeeSlava);
        Assert.assertTrue(response.asString().equals("<html>\n" +
                "  <body>{\"success\":{\"text\":\"successfully! deleted Records\"}}</body>\n" +
                "</html>"));
    }

    @Test
    public void deleteInexistingEmployeeTst() {
        //test data prepare
        Employee inexistingEmployee = new Employee();
        inexistingEmployee.setId(DataGenerator.makeRandomName());

        //test
        Response response = apiManager.deleteEmployee(inexistingEmployee);
        response.prettyPrint();

        Assert.assertTrue(response.asString().equals("<html>\n" +       //looks like we can`t to view the error
                "  <body>{\"success\":{\"text\":\"successfully! deleted Records\"}}</body>\n" +
                "</html>"));
    }
}
