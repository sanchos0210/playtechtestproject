import com.fasterxml.jackson.databind.ObjectMapper;
import models.Context;
import org.testng.annotations.BeforeMethod;
import java.io.File;
import java.io.IOException;

public class BaseTest {

    protected Context context;
    private ObjectMapper mapper = new ObjectMapper();


    @BeforeMethod
    public void setUp() throws IOException {
        context = mapper.readValue(new File("src/test/resources/test_data.json"), Context.class);
    }

}
