import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import exeptions.JsonParsingException;
import jdk.internal.org.objectweb.asm.TypeReference;
import models.Employee;
import models.ResponseWrapper;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Serializer<E> {

    private ObjectMapper mapper = new ObjectMapper();
    private XmlMapper xmlMapper = new XmlMapper();


    public E unwrapResponse(String response) throws IOException {
        ResponseWrapper responseWrapper = xmlMapper.readValue(response, ResponseWrapper.class);

        return (E) mapper.readValue(responseWrapper.getBody(), Employee.class);
    }

    public E unwrapJsonResponse(String response) throws IOException {
        return (E) mapper.readValue(response, Employee.class);
    }


    public String serializeToString(E entity) throws JsonParsingException {
        try {
            return mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, true).writeValueAsString(entity);
        } catch (JsonProcessingException e) {
            throw new JsonParsingException("Error while deserialization", e);
        }
    }

    public void writeToFile(File file, List<E> entityList) throws IOException {
        mapper.writeValue(file, entityList);
    }
}
