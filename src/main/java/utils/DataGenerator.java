package utils;

import org.apache.commons.lang3.RandomStringUtils;

public class DataGenerator {

    public static String makeRandomName() {
        return "AT_name_" + RandomStringUtils.randomAlphabetic(9) + "_" + createNumericString("0", 4);
    }

    /**
     * Create random numeric string with total length and specified prefix
     *
     * @param prefix - prefix e.g. phone number prefix
     * @param length - total length of generated string
     * @return numeric string
     */
    public static String createNumericString(String prefix, int length) {
        if (prefix == null) {
            throw new IllegalArgumentException("prefix cannot be null");
        }
        if (prefix.length() > length) {
            throw new IllegalArgumentException("prefix cannot be greater than length of generated string");
        }
        return prefix + RandomStringUtils.randomNumeric(length - prefix.length());
    }

}
