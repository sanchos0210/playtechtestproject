package models;

public class ResponseWrapper {

    private String body;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "ResponseWrapper{" +
                "body='" + body + '\'' +
                '}';
    }
}
