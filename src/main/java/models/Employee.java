package models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Employee {

    private String id;

    private String name;

    private String salary;

    private String age;

    @JsonProperty(value = "profile_image")
    private String profileImage;


    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSalary() {
        return salary;
    }

    public String getAge() {
        return age;
    }

    @JsonGetter(value = "profile_image")
    public String getProfileImage() {
        return profileImage;
    }


    public Employee setId(String id) {
        this.id = id;
        return this;
    }

    public Employee setName(String name) {
        this.name = name;
        return this;
    }

    public Employee setSalary(String salary) {
        this.salary = salary;
        return this;
    }

    public Employee setAge(String age) {
        this.age = age;
        return this;
    }

    @JsonSetter(value = "employee_name")
    public Employee setEmployeeName(String name) {
        this.name = name;
        return this;
    }

    @JsonSetter(value = "employee_salary")
    public Employee setEmployeeSalary(String salary) {
        this.salary = salary;
        return this;
    }

    @JsonSetter(value = "employee_age")
    public Employee setEmployeeAge(String age) {
        this.age = age;
        return this;
    }


    @JsonSetter(value = "profile_image")
    public Employee setProfileImage(String profileImage) {
        this.profileImage = profileImage;
        return this;
    }

    @Override
    public boolean equals(Object o) {   //simple implementation of equals method, it not finished yet
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return id.equals(employee.id) &&
                name.equals(employee.name) &&
                salary.equals(employee.salary) &&
                age.equals(employee.age);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + salary.hashCode();
        result = 31 * result + age.hashCode();
        result = 31 * result + profileImage.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "models.Employee{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", salary='" + salary + '\'' +
                ", age='" + age + '\'' +
                ", profileImage='" + profileImage + '\'' +
                '}';
    }
}
