package models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Context {

    @JsonProperty(value = "first_employee")
    private Employee first;

    @JsonProperty(value = "second_employee")
    private Employee second;

    @JsonProperty(value = "third_employee")
    private Employee third;


    public Employee getFirst() {
        return first;
    }

    public Employee getSecond() {
        return second;
    }

    public Employee getThird() {
        return third;
    }


    public Context setFirst(Employee first) {
        this.first = first;
        return this;
    }

    public Context setSecond(Employee second) {
        this.second = second;
        return this;
    }

    public Context setThird(Employee third) {
        this.third = third;
        return this;
    }


    @Override
    public String toString() {
        return "Context{" +
                "first=" + first +
                ", second=" + second +
                ", third=" + third +
                '}';
    }
}
