import exeptions.JsonParsingException;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import models.Employee;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EmployeeApiManager {

    public Employee getEmployee(String id) throws IOException {
        Response response = RestAssured.given()
                .get("http://dummy.restapiexample.com/api/v1/employee/" + id);
        response.prettyPrint();

        return new Serializer<Employee>().unwrapResponse(response.asString());
    }

    public List<Employee> getEmployees(List<String> ids) throws IOException {
        List<Employee> receivedEmployees = new ArrayList<Employee>();
        for(String id: ids) {
            receivedEmployees.add(getEmployee(id));
        }
        return receivedEmployees;
    }


    public Employee createEmployee(Employee employee) throws IOException, JsonParsingException {
        Response response = RestAssured.given()
                .body(new Serializer<Employee>().serializeToString(employee))
                .post("http://dummy.restapiexample.com/api/v1/create");
        response.prettyPrint();
        return new Serializer<Employee>().unwrapResponse(response.asString());
    }

    public List<Employee> createEmployees(List<Employee> employees) throws IOException, JsonParsingException {
        List<Employee> createdEmployees = new ArrayList();
        for(Employee employee: employees) {
            createdEmployees.add(createEmployee(employee));
        }
        return createdEmployees;
    }

    public Employee updateEmployeeData(Employee employee) throws IOException, JsonParsingException {
        Response response = RestAssured.given()
                .body(new Serializer<Employee>().serializeToString(employee))
                .put("http://dummy.restapiexample.com/api/v1/update/" + employee.getId());
        response.prettyPrint();
        return new Serializer<Employee>().unwrapResponse(response.asString());
    }

    public Response deleteEmployee(Employee employee) {
        Response response = RestAssured.given()
                .delete("http://dummy.restapiexample.com/api/v1/delete/" + employee.getId());
        response.prettyPrint();
        return response;
    }
}
